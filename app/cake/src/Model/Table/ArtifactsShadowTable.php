<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsShadow Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 *
 * @method \App\Model\Entity\ArtifactsShadow get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsShadow newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtifactsShadow[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsShadow|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsShadow|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsShadow patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsShadow[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsShadow findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtifactsShadowTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artifacts_shadow');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('cdli_comments')
            ->allowEmpty('cdli_comments');

        $validator
            ->scalar('collection_location')
            ->maxLength('collection_location', 100)
            ->allowEmpty('collection_location');

        $validator
            ->scalar('collection_comments')
            ->maxLength('collection_comments', 100)
            ->allowEmpty('collection_comments');

        $validator
            ->scalar('acquisition_history')
            ->allowEmpty('acquisition_history');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));

        return $rules;
    }
}
