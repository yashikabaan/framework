// Set default margins, width and height
var margin = {top: 50, right: 50, bottom: 50, left: 50};
var width = 1200 - margin.left - margin.right;
var height = 800 - margin.top - margin.bottom;

// Define Zoom event
var zoom = d3.zoom()
    .scaleExtent([1, 10])
    .on("zoom", zoomed)

// Add SVG to Choropleth map container and append a group to it
var svg = d3.select("#choropleth-map")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .call(zoom)
        .call(zoom.transform, d3.zoomIdentity.translate(-1700, -800).scale(4))
            .append("g")
                .attr("transform","translate(-1700, -800) scale(4, 4)");

// Helper function for Zoom
function zoomed() {
    if (svg)
        svg.attr("transform", d3.event.transform);
}


// Define Map and projection
var path = d3.geoPath();
var projection = d3.geoMercator()
  .scale(100)
  .center([0,20])
  .translate([width / 2, height / 2]);

// Define color scale
var colorScale = d3.scaleOrdinal(d3.schemeBlues[7])

// Define tooltip to display information about the region/proveniences
var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("background", "#000")
    .style("color", "#FFF")
    .style("padding", "6px")
    .style("white-space", "pre-wrap")
    .text("");


//function ready(data) {
//	// Draw the map
//	svg.append("g")
//		.selectAll("path")
//    	.data(data.features)
//    	.enter()
//    	.append("path")
//      		// Draw each provenience
//      		.attr("d", d3.geoPath()
//        		  .projection(projection)
//      		)
//      		// Set the color of each provenience
//      		.attr("fill", function (d) {
//				return colorScale(d.properties.regionId)
//      		})
//        .on("mouseover", function(d) {
//            // Add tooltip to be displayed when any provenience is hovered
//            tooltip.text("Provenience : " + d.properties.provenience + "\n" + "Region : " + d.properties.region);
//            return tooltip.style("visibility", "visible");
//        })
//        .on("mousemove", function() {
//            return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
//        })
//        .on("mouseout", function() {
//            return tooltip.style("visibility", "hidden");
//        });    
//}

function ready(data) {
	// Draw the map
	svg.append("g")
		.selectAll("path")
    	.data(data.features)
    	.enter()
    	.append("path")
      		// Draw each region
      		.attr("d", d3.geoPath()
        		.projection(projection)
      		)
      		// Set the color of each region
      		.attr("fill", function (d) {
				return colorScale(d.properties.region)
      		})
        .on("mouseover", function(d) {
            // Add tooltip to be displayed when any region is hovered
            tooltip.text("Region : " + d.properties.region);
            return tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() {
            return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
        })
        .on("mouseout", function() {
            return tooltip.style("visibility", "hidden");
        });    
}

d3.json("http://127.0.0.1:2354/assets/map/world.geojson").then(function(backgroundMap) {
    // Draw the background World map
    svg.append("g")
        .selectAll("path")
        .data(backgroundMap.features)
        .enter()
        .append("path")
            // Draw each country
            .attr("d", d3.geoPath()
                .projection(projection)
            )
            // Set the color of each country
            .attr("fill", "#EAEAEA")
    
    // Plot the regions/proveniences after the background map is loaded
    ready(data);
})